@extends('layouts.master')
<!DOCTYPE html>
<html>
<body>
<header>
  
</header>

@section('content')
<h2>API Corona testing</h2>

<table style="width:100%">
  <tr>
    <th>Country</th>
    <th>Country Code</th> 
    <th>Total Confirmed</th>
    <th>Total Deaths</th>
  </tr>
  @foreach($corona['Countries'] as $nadircok)
		<tr>
			<td>{{ $nadircok['Country'] }}</td>
			<td>{{ $nadircok['CountryCode'] }}</td>
			<td>{{ $nadircok['TotalConfirmed'] }}</td>
			<td>{{ $nadircok['TotalDeaths'] }}</td>
		</tr>
	@endforeach
 
</table>

<table style="width:100%">
  <tr>
    <th>Total Confirmed</th>
    <th>Total Deaths</th> 
    <th>Total Recovered</th>
  </tr>
		<tr>
			<td>{{ $corona['Global']['TotalConfirmed'] }}</td>
			<td>{{ $corona['Global']['TotalDeaths'] }}</td>
			<td>{{ $corona['Global']['TotalRecovered'] }}</td>
		</tr>
 
</table>
@endsection

</body>
</html>

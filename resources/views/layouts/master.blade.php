<!DOCTYPE html>
<html lang="en">
<head>
<title>Corona Update</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="{{asset('css/master.css')}}">
<link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
</head>
<body>
<header> 
<div class="header">
<div class="header-contentA">
<i style="font-size: 20px" class="icofont-medical-sign"> Inpedia Corona Virus Update</i>
</div>
    <div class="header-content">
        <i style="font-size: 20px" class="icofont-instagram"></i>
        <i style="font-size: 20px" class="icofont-facebook"></i>
        <i style="font-size: 20px" class="icofont-brand-whatsapp"></i>
        <i style="font-size: 20px" class="icofont-line"></i>
    </div>
</div>

</header>
<navbar>
<nav>
    <ul>
        <a class="menu" href=""> Home </a>
        <a class="menu" href=""> Global </a>
        <a class="menu" href=""> Countries </a>
        <a class="menu" href=""> News </a>
    </ul>
</nav>
</navbar>
<br>

<section>
    @yield('content')
</section>
<footer>
<div class="borderfooter">
<center><p class="borderA"  style="font-size : 18px; font-style: sans-serif"> <i class="icofont-copyright"></i> copyright Inpedia Corona Virus Update </p></center>
</div>
</footer>
</body>

<script src="{{asset('js/app.js')}}"></script>
@yield('js')

</html>
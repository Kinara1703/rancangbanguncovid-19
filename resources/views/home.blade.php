@extends('layouts.master')

<link rel="stylesheet" href="{{asset('css/home.css')}}">
<body>

@section('content')
<div class="top-content">
<!-- Slideshow container -->

<div class="leftcontent"> 
<h1> Judul </h1>
<p> lorem ipsum, lorem ipsum, lorem ipsum,lorem ipsum,lorem ipsum<p>
<p>lorem ipsum,lorem ipsum,lorem ipsum,lorem ipsum <p>
</div>
<div class="slideshow-container">

  <!-- Full-width images with number and caption text -->
  <div class="mySlides fade">
    <div class="numbertext">1 / 3</div>
    <img src="img/gambar1.jpg" style="width:75%; height: 400px">
   
  </div>

  <div class="mySlides fade">
    <div class="numbertext">2 / 3</div>
    <img src="img/gambar1.jpg"  style="width:75%; height: 400px">
 
  </div>

  <div class="mySlides fade">
    <div class="numbertext">3 / 3</div>
    <img src="img/gambar1.jpg"  style="width:75%; height: 400px">
  </div>



<!-- The dots/circles -->
<br>
<br>
<div style="text-align:center">
  <span class="dot" onclick="currentSlide(1)"></span>
  <span class="dot" onclick="currentSlide(2)"></span>
  <span class="dot" onclick="currentSlide(3)"></span>
</div>
</div>
</div>

<div class="middle-content">
</div>

@endsection

</body>
@section('js')
<script>
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 5000); // Change image every 2 seconds
}
</script>
@endsection


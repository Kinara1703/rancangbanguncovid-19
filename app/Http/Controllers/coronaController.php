<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use GuzzleHttp\Client;
use App\Http\Requests;

class coronaController extends Controller
{
  public function corona(){
    $client = new Client();
    $request = $client->get('https://api.covid19api.com/summary');
    $response = $request->getBody()->getContents();
    $corona = json_decode($response, true);


    return view('confirmed',compact('corona'));
  }
  public function home(){
    return view('home');
  }
}
